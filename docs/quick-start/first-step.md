---
prev: false
next:
  text: '简单介绍各个应用'
  link: 'quick-start/a-brief-introdction-for-applications'
---

# 第一步

啊，第一步。第一步就是打开浏览器。然后输入网址：https://dashy.tong-ju.top:8443

当网页打开的一刻，你就看到了这个服务器的首页。这个工具箱就归你所有了。

:::info
如果无法访问，有时是由于运营商变更公网ip，致使 你使用的DNS、本地DNS缓存 还未更新（这个问题尚在解决中）所导致的。

> 从之前的日志看，可能会持续1个小时左右。
:::

:::info 推荐
[Dashy](https://dashy.to/) 是一款开源、高度可定制、易于使用、尊重隐私的仪表板应用程序。

我们用它搭建了服务器的主页。
:::