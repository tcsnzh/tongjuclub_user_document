---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "二本科技使用指南"
  text: ""
  tagline: 给家人们介绍一下我们服务器上的应用的使用方法。
  actions:
    - theme: brand
      text: 快速入门 →
      link: /quick-start/first-step

features:
  - title: 此教程使用 VitePress 搭建
    details: VitePress 是一个文档静态页面生成器。生成的页面访问起来超级快。
  - title: 工作人员（仅收件）邮箱
    details: 点击发送邮件。
    link: 'mailto:05675e44c042f08d06d468b09610b8aa.show-sender.include-footer.include-quotes@streams.zulipchat.com'
---
